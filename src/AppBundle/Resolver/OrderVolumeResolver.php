<?php

namespace AppBundle\Resolver;

use AppBundle\Model\OrderInterface;

/**
 * Class OrderVolumeResolver
 */
class OrderVolumeResolver
{
    /**
     * @var array
     */
    protected $volumes;

    /**
     * OrderVolumeResolver constructor.
     *
     * @param array $volumes
     */
    public function __construct(array $volumes)
    {
        $this->volumes = $volumes;
    }

    /**
     * @return float
     * @throws \Exception
     */
    public function resolve(OrderInterface $order)
    {
        $volume = $order->getVolume();

        foreach ($this->volumes as $upperVolume) {
            if ($volume < $upperVolume) {
                return $upperVolume;
            }
        }

        throw new \Exception('Order volume can be resolved');
    }
}
