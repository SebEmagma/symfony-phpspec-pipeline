<?php

namespace AppBundle\Model;

/**
 * Interface OrderInterface
 */
interface OrderInterface
{
    /**
     * @return float
     */
    public function getVolume();
}
