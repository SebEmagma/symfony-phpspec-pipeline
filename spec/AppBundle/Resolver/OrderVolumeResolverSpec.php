<?php

namespace spec\AppBundle\Resolver;

use AppBundle\Model\OrderInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OrderVolumeResolverSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith([
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.8,
            1,
            1.5,
            2,
        ]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Resolver\OrderVolumeResolver');
    }

    function it_resolves_an_order_volume(OrderInterface $order)
    {
        $order->getVolume()->shouldBeCalled()->willReturn(0.05);
        $this->resolve($order)->shouldReturn(0.1);

        $order->getVolume()->shouldBeCalled()->willReturn(0.55);
        $this->resolve($order)->shouldReturn(0.8);

        $order->getVolume()->shouldBeCalled()->willReturn(1.25);
        $this->resolve($order)->shouldReturn(1.5);
    }

    function it_throws_an_exception_for_too_big_order_volume(OrderInterface $order)
    {
        $order->getVolume()->shouldBeCalled()->willReturn(10);
        $this->shouldThrow('\Exception')->duringResolve($order);
    }
}
